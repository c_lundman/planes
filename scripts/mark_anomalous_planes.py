#!/usr/bin/env python

import argparse
import csv

class PlaneModel():
    def __init__(self, model):
        self.model = model
        self.seatss = []
        self.uids = []
        self.markeds = []

    def add_plane(self, uid, seats):
        self.uids.append(uid)
        self.seatss.append(seats)
        self.markeds.append(False)

    def mark_uid(self, uid):
        self.markeds[self.uids.index(uid)] = True

    def get_model(self):
        return self.model

    def get_number_of_planes(self):
        return len(self.uids)

    def get_max_seats_number(self):
        return max(self.seatss)

    def get_uids(self):
        return self.uids

    def get_marked_uids(self):
        return [self.uids[i] for i in range(len(self.uids)) if self.markeds[i]]

    def get_seats_list(self):
        return self.seatss

    def get_marked_seats_list(self):
        return [self.seatss[i] for i in range(len(self.uids)) if self.markeds[i]]

    def get_unmarked_seats_list(self):
        return [self.seatss[i] for i in range(len(self.uids)) if not self.markeds[i]]

    def get_marked_fraction(self):
        nr_marked = 0
        for marked in self.markeds:
            if marked:
                nr_marked += 1
        return float(nr_marked)/len(self.markeds)

    def get_output_strings(self):
        header_string = "model, median seats in model, uid, seats for this uid\n"
        strings = []
        median_seats = get_median(self.seatss)
        for uid, seats, marked in zip(self.uids, self.seatss, self.markeds):
            if marked:
                string = '%s, %i, %i, %i\n' % (self.model, median_seats, uid, seats)
                strings.append(string)
        return strings, header_string

def load_data(filename):
    model_names = []
    plane_models = []
    with open(filename, 'r') as f:
        csv_reader = csv.reader(f, delimiter=',')
        next(csv_reader)
        for row in csv_reader:

            # Parse the file
            uid = int(row[0])
            airline = row[1][:2]
            model = row[2]
            config = row[3]
            dates = row[4]

            # Extract nr_seats from config
            nr_seats = 0
            try:
                dictionary = eval(config)
                for seat_type in dictionary:
                    nr_seats += int(dictionary[seat_type])
            except:
                pass

            # Store data
            if model not in model_names:
                model_names.append(model)
                plane_model = PlaneModel(model)
                plane_model.add_plane(uid, nr_seats)
                plane_models.append(plane_model)
            else:
                plane_models[model_names.index(model)].add_plane(uid, nr_seats)

    return plane_models

def get_median(xs):
    i = int(.5*len(xs))
    x_median = sorted(xs)[i]
    return x_median

def mark_planes(plane_models, min_seats, median_fraction):
    # Mark plane if number of seats is less than half of median seats number
    # Also mark all planes with less than 10 seats

    for plane_model in plane_models:

        # Data
        uids = plane_model.get_uids()
        seats_list = plane_model.get_seats_list()

        # Find median
        seats_median = get_median(seats_list)

        # Mark planes
        for uid, seats in zip(uids, seats_list):
            if seats < min_seats or seats < median_fraction*seats_median:
                plane_model.mark_uid(uid)
    return plane_models

def get_marked_uids(plane_models):
    marked_uids = []
    for plane_model in plane_models:
        marked_uids += plane_model.get_marked_uids()
    return sorted(marked_uids)

def save_output(plane_models, output_file):
    with open(output_file, 'w') as f:
        for i, plane_model in enumerate(plane_models):
            # Get output
            strings, header_string = plane_model.get_output_strings()

            # If first plane, write header
            if not i:
                f.write(header_string)

            # Write data
            for string in strings:
                f.write(string)

def get_args():
    # Default parameters
    min_seats_default = 10
    median_fraction_default = 0.5

    # Make the argument parser
    parser = argparse.ArgumentParser(description='a script that finds anomalous plane seat information from an input data file (CSV_FILE), and (optionally) saves the UID of the marked planes to an output file. the script can also plot histograms of the marked and unmarked data (requires matplotlib).')
    parser.add_argument('INPUT_CSV_FILE', help='the input data file')
    parser.add_argument('-ms', '--mark-min-seats', type=int, default=min_seats_default, help='marks all planes with a seat number smaller than MARK_MIN_SEATS [default=%i]' % (min_seats_default))
    parser.add_argument('-mf', '--mark-median-fraction', type=float, default=median_fraction_default, help='marks all planes with a seat number smaller than MARK_MEDIAN_FRACTION*MEDIAN, where MEDIAN is the median seat number for that plane model [default=%3.1f]' % (median_fraction_default))
    parser.add_argument('-o', '--output-csv-file', type=str, help='save a column of the marked plane UIDs to this file')
    # parser.add_argument('-f', '--fraction-marked', action='store_true', help='print the fraction of planes marked for each model')
    parser.add_argument('-p', '--plot', default=False, nargs='?', help='shows a histogram for each plane model for visual inspection, with marked planes in red. if a (optional) number is given, show only that many models. requires matplotlib to be installed')
    args = parser.parse_args()

    # If the plotting argument is given without a number, it should show all plane models
    if args.plot == None:
        args.plot = 1000000
    if args.output_csv_file == None and args.plot == False:
        print("Please use either --plot or --output-file (all options can be viewed with --help).")
        raise SystemExit
    return args

def main():
    # Load data
    args = get_args()
    plane_models = load_data(args.INPUT_CSV_FILE)

    # Mark planes
    plane_models = mark_planes(plane_models, min_seats=args.mark_min_seats, median_fraction=args.mark_median_fraction)

    # # Print marked fraction?
    # if args.fraction_marked:
    #     for plane_model in plane_models:
    #         print('%s: %6.4f' % (plane_model.get_model(), plane_model.get_marked_fraction()))

    # Save output file?
    if args.output_csv_file:
        save_output(plane_models, args.output_csv_file)

    # Plot the data?
    if args.plot:

        # Importing matplotlib
        import matplotlib.pyplot as plt
        plt.style.use('bmh')
        plt.rcParams.update({'font.size': 22})

        # Looping over models
        for j, plane_model in enumerate(plane_models):
            if j >= int(args.plot):
                break

            # Plot histogram
            plt.figure(figsize=(12, 8))
            max_nr_seats = plane_model.get_max_seats_number()
            bins = [i-.5 for i in range(max_nr_seats+2)]
            plt.hist(plane_model.get_unmarked_seats_list(), bins=bins, color='DodgerBlue', zorder=2)
            plt.hist(plane_model.get_marked_seats_list(), bins=bins, color='r', zorder=3, label=r'$\mathrm{Marked}$')

            # Make the plot look good
            plt.xlim(bins[0]-1, max(11.5, bins[-1]+1))
            plt.xlabel(r'$\mathrm{%s: \, Number \, of \, seats \, (%i/%i)}$' % (plane_model.get_model(), j+1, len(plane_models)))
            plt.ylabel(r'$\mathrm{Number \, of \, planes}$')
            plt.legend(loc='best')
            plt.tight_layout()
            plt.show()
            plt.close()

if __name__ == '__main__':
    main()
